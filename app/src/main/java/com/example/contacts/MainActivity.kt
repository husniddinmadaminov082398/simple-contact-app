package com.example.contacts

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import com.example.contacts.Dialogs.AddDialogStu
import com.example.contacts.Dialogs.EditDialogS
import com.example.contacts.adapters.ContactAdapter
import com.example.contacts.database.Database
import com.example.contacts.models.ContactData
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_confirmstudent.*

/**
 * Created by Husniddin Madaminov on 07.05.2019
 * Tashkent, Uzbekistan.
 */


class MainActivity : AppCompatActivity() {
    private lateinit var adapter: ContactAdapter
    private lateinit var data: ArrayList<ContactData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val database = Database.getDatabase()
        data = database.getContacts()
        adapter = ContactAdapter(data)
        list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        list.adapter = adapter

            addcontact.setOnClickListener {
                val dialog = AddDialogStu(this)
                dialog.setOnClickListener { name, phone ->
                    val d = ContactData(0, name, phone)
                    data.add(d)
                    database.addContact(d)
                    adapter.notifyDataSetChanged()
                }
                dialog.show()
            }

        adapter.setOnDeleteClickListener {
            android.app.AlertDialog.Builder(this)
                .setTitle("Delete")
                .setMessage("Do you want delete?")
                .setPositiveButton("YES") { dialog, which ->
                    database.deleteContact(data[it].id)
                    data.removeAt(it)
                    adapter.notifyItemRemoved(it)

                }
                .setNegativeButton("NO", { dialog, which -> })
                .show()
        }

        adapter.setOnEditClickListener {
            val dialog = EditDialogS(this, data[it])
            dialog.setOnClickListener { name, phone ->
                data[it].name = name
                data[it].number = phone
                database.updateContact(data[it])
                adapter.notifyDataSetChanged()
            }
            dialog.show()
        }

        adapter.setOnCallClickListener {
            Log.d("TTT", "onCallClick")
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + (data[it].number))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivityForResult(intent, 1)
        }
    }
}
