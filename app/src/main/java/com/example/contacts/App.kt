package com.example.contacts

import android.app.Application
import com.example.contacts.database.Database

/**
 * Created by Husniddin Madaminov on 07.05.2019
 * Tashkent, Uzbekistan.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Database.init(this)
    }

    override fun onTerminate() {
        super.onTerminate()
        Database.getDatabase().closeDatabase()
    }
}