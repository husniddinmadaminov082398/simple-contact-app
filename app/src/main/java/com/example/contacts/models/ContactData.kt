package com.example.contacts.models

/**
 * Created by Husniddin Madaminov on 07.05.2019
 * Tashkent, Uzbekistan.
 */

data class ContactData(
    var id:Long,
    var name: String,
    var number: String
){
    companion object
}