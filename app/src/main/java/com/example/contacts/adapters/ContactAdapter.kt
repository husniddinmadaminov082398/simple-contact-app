package com.example.contacts.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.example.contacts.R
import com.example.contacts.models.ContactData
import kotlinx.android.synthetic.main.item_contact.view.*

/**
 * Created by Husniddin Madaminov on 07.05.2019
 * Tashkent, Uzbekistan.
 */

class ContactAdapter(val data: ArrayList<ContactData>) :
    androidx.recyclerview.widget.RecyclerView.Adapter<ContactAdapter.ViewHolder>() {
    private var editClickListener: ItemClick2? = null
    private var deleteClickListener: ItemClick2? = null
    private var callClickListener: ItemClick2? = null
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ContactAdapter.ViewHolder = ViewHolder(
        p0.inflate(
            R.layout.item_contact
        )
    )

    override fun getItemCount() = data.size

    override fun onBindViewHolder(p0: ContactAdapter.ViewHolder, p1: Int) = p0.bind()
    fun setOnEditClickListener(f: ItemClick2) {
        editClickListener = f
    }

    fun setOnCallClickListener(f: ItemClick2) {
        callClickListener = f
    }


    fun setOnDeleteClickListener(f: ItemClick2) {
        deleteClickListener = f
    }

    inner class ViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        init {

            view.calltargetphone.setOnClickListener {
                callClickListener?.invoke(adapterPosition)
            }

            view.buttonDelete.setOnClickListener {
                deleteClickListener?.invoke(adapterPosition)
                view.swipeLayout.close(true)
            }

            view.buttonEdit.setOnClickListener {
                editClickListener?.invoke(adapterPosition)
                view.swipeLayout.close(true)
            }

        }

        fun bind() {
            itemView.apply {
                val d = data[adapterPosition]
                studentName.text = d.name
                studentNumber.text = d.number
            }
        }
    }

}

fun ViewGroup.inflate(@LayoutRes resId: Int) =
    LayoutInflater.from(context).inflate(resId, this, false)

typealias ItemClick2 = (Int) -> Unit