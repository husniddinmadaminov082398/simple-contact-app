package com.example.contacts.Dialogs

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import com.example.contacts.R
import com.example.contacts.models.ContactData
import kotlinx.android.synthetic.main.dialog_confirmstudent.view.*

/**
 * Created by Husniddin Madaminov on 07.05.2019
 * Tashkent, Uzbekistan.
 */

class EditDialogS(val context: Context, data: ContactData) {
    private val dialog = AlertDialog.Builder(context).create()
    private var listener: ((String, String) -> Unit)? = null

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_confirmstudent, null, false)

        view.inputStudentName.setText(data.name)
        view.inputPhone.setText(data.number)
        dialog.setTitle("Edit Informations")
        dialog.setView(view)
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Edit") { dialog, which ->
            listener?.invoke(view.inputStudentName.text.toString(), view.inputPhone.text.toString())
        }
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel") { dialog, which -> }
    }

    fun setOnClickListener(f: (String, String) -> Unit) {
        listener = f
    }

    fun show() {
        dialog.show()
    }
}

