package com.example.contacts.Dialogs

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.example.contacts.R
import kotlinx.android.synthetic.main.dialog_confirmstudent.view.*

/**
 * Created by Husniddin Madaminov on 07.05.2019
 * Tashkent, Uzbekistan.
 */

class AddDialogStu(context: Context) {
    private var dialog = AlertDialog.Builder(context).create()
    private var listener: ((String, String) -> Unit)? = null
    private var selectPosition = 0

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_confirmstudent, null, false)

        dialog.setView(view)
        dialog.setTitle("Add Contact")
        dialog.setButton(
            DialogInterface.BUTTON_POSITIVE, "Add"

        ) { _, _ ->
            listener?.invoke(
                view.inputStudentName.text.toString(), view.inputPhone.text.toString()
            )
        }
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel") { _, _ -> }
    }

    fun setOnClickListener(f: (String, String) -> Unit) {
        listener = f
    }

    fun show() {
        dialog.show()
    }
}