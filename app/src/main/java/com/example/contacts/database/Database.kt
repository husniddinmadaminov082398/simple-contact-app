package com.example.contacts.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import com.example.contacts.models.ContactData

/**
 * Created by Husniddin Madaminov on 07.05.2019
 * Tashkent, Uzbekistan.
 */

class Database private constructor(context: Context) : DBHelper(context, "contacts.db") {

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var database: Database

        fun init(context: Context) {
            database = Database(context)
        }
        fun getDatabase() = database
    }

    fun addContact(contactData: ContactData) {
        val cv = ContentValues()
        cv.put("name", contactData.name)
        cv.put("number", contactData.number)
        val id = database().insert("contacts", null, cv)
        contactData.id = id
    }

    fun updateContact(contactData: ContactData) {
        val cv = ContentValues()
        cv.put("name", contactData.name)
        cv.put("number", contactData.number)
        database().update("contacts", cv, "_id=?", arrayOf(contactData.id.toString()))
    }

    fun deleteContact(id:Long) =database().delete("contacts", "_id=?", arrayOf(id.toString()))

    fun getContacts(): ArrayList<ContactData> {
        val contacts = ArrayList<ContactData>()
        val cursor = database().rawQuery("SELECT * FROM contacts", null)
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast) {
                val contactData = ContactData(
                    cursor.getLong(cursor.getColumnIndex("_id")),
                    cursor.getString(cursor.getColumnIndex("name")),
                    cursor.getString(cursor.getColumnIndex("number"))
                )
                contacts.add(contactData)
                cursor.moveToNext()
            }
        }
        cursor.close()
        return contacts
    }
}

/*
fun ContactData.save() = Database.getDatabase().addContact(this)
fun ContactData.update() = Database.getDatabase().updateContact(this)
fun ContactData.Companion.getAll() = Database.getDatabase().getContacts()*/
